const http = require('http');
const express = require('express');
const { dirname } = require('path')
const app = express();
const bodyparser = require("body-parser");
const misrutas = require('./router/index');
const path = require('path');


app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

// app.use(bodyparser.urlencoded({extended:true}));
app.use(express.json());
app.use(misrutas);

app.engine('html',require('ejs').renderFile);



app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html')
});

//  escuchar el servidor por el puerto 3004
const puerto = 3004;
app.listen(puerto,() => {
    
    console.log("iniciado en el puerto 3004");


});
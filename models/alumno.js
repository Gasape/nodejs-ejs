const json = require('express/lib/response');
const response = require('path/posix');
const promise = require('../models/conexion.js');
const conexion = require('../models/conexion.js');


var AlumnoDB= {}
let alumno = {
    matricula: "",
    nombre: "",
    domicilio: "",
    sexo: "",
    especialidad: ""
}

AlumnoDB.insertar = function insertar(alumno){

    return new Promise ((resolve,reject) => {

        var sqlConsulta = "insert into alumnos set ?";

        conexion.query(sqlConsulta,alumno,function(err,res){
            if(err){
                reject(err.message);
            }
            else{
                resolve({
                    id:res.insertId,
                    matricula:alumno.matricula,
                    nombre:alumno.nombre,
                    domicilio:alumno.domicilio,
                    sexo:alumno.sexo,
                    especialidad:alumno.especialidad

                });
            }
        });
    });


}

AlumnoDB.mostrarTodos = function mostrarTodos(){
    return new Promise ((resolve,reject)=>{
        alumno = {}
        var sqlConsulta = " select * from alumnos;";

        conexion.query(sqlConsulta,null,function(err,res){
            if(err){
                reject(err.message);
            }
            else{
                alumno = res;
                
                resolve(alumno)
            }
        });
    });
}

// buscar por matricula

AlumnoDB.mostrarPorMatricula = function mostrarPorMatricula(matricula){
    return new Promise ((resolve,reject)=>{
        alumnos = {};
        var sqlConsulta = " select * from alumnos where matricula = ? ;";

        conexion.query(sqlConsulta,matricula,function(err,res){
            if(err){
                return reject(err)   
            }
            if(res.length<=0){
                console.log("No existe ese registro");
                return resolve;
            }
            else{
                alumnos = res;

                resolve(res)
            }
        });
    });
}

// borrar por matricula
AlumnoDB.borrarPorMatricula = function borrarPorMatricula(matricula){
    return new Promise ((resolve,reject)=>{
        var sqlConsulta = "DELETE from alumnos where matricula = ? ;";

        conexion.query(sqlConsulta,matricula,function(err,res){
            if(err){
                reject(err.message);
            }
            else{
                console.log(res.affectedRows);
                resolve(res.affectedRows);
            }
        });
    });
}
AlumnoDB.actualizarAlumno = function actualizarAlumno(alumno){
    return new Promise ((resolve,reject)=>{
        var sqlConsulta = "UPDATE alumnos SET nombre = ?, domicilio = ?, sexo = ?, especialidad = ?   where matricula = ?;";

        conexion.query(sqlConsulta,[alumno.nombre, alumno.domicilio, alumno.sexo, alumno.especialidad, alumno.matricula],function(err,res){
            if(err){
                reject(err.message);
            }
            else{
                console.log(res.affectedRows);
                resolve(res.affectedRows);
            }
        });
    });
}
module.exports = AlumnoDB; 
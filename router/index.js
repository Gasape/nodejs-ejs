const express = require('express');
const router = express.Router();
const bodyParse = require('body-parser');
const db = require('../models/alumno.js')

let alumno = {
    matricula: "",
    nombre: "",
    domicilio: "",
    sexo: "",
    especialidad: ""
}

router.post("/insertar",async(req,res)=>{

    alumno ={
        matricula:req.body.matricula,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        sexo:req.body.sexo,
        especialidad:req.body.especialidad

    }
    
  let resultado = await db.insertar(alumno);
  res.json(resultado);
})
router.get('/mostrarTodos', async(req,res)=>{

    resultado = await db.mostrarTodos();
    res.json(resultado)
})

router.get('/buscar',(req,res)=> {
    const matricula = req.query.matricula;
    db.mostrarPorMatricula(matricula)
        .then((data) => res.json(data))
        .catch((err) => res.status(404).send(err.mesagge)) 
})

router.delete('/borrar',(req,res)=> {
    const matricula = req.query.matricula;
    db.borrarPorMatricula(matricula)
        .then((data) => res.json(data))
        .catch((err) => res.status(404).send(err.mesagge)) 
})
router.put('/actualizar',(req,res)=> {
    const alumno = req.body;
    db.actualizarAlumno(alumno)
        .then((data) => res.json(data))
        .catch((err) => res.status(404).send(err.mesagge)) 
})

// Declaracion de arreglo de alumnos
let datos =[{
    
    matricula: "2020030330",
    nombre: "Gabriel Santoyo Peña",
    sexo: "H",
    materias: ["ingles","Tecnologia 1", "Base de datos"] 

},{

matricula: "2020030737",
nombre: "Brian Eduardo Rios Muñoz",
sexo: "H",
materias: ["ingles","Tecnologia 1", "Base de datos"] 

},
{

matricula: "2020030312",
nombre: "Lizbeth Argelia Padilla Moreno",
sexo: "F",
materias: ["ingles","Tecnologia 1", "Base de datos"] 

}]


router.get('/index', (req,res)=>{

    res.render('index.html',)

})
router.post('/index', (req,res) =>{

    res.render('index.html')
})


router.get('/tablas', (req,res)=>{
    const valores = {
        tablas:req.query.number
    }   
    res.render('tablas.html',valores);
})

router.post("/tablas",(req,res)=>{
    const valores={
        tablas:req.body.tablas
    }
    res.render('tablas.html',valores);
})

router.get('/cotizacion', (req,res)=>{
    const valores = {
        plazos:req.query.plazos,
        pInicial:req.query.pInicial,
        valor:req.query.valor,
    }
    res.render('cotizacion.html',valores);
})

router.post("/cotizacion",(req,res)=>{
    const valores={
        plazos:req.body.plazos,
        valor:req.body.valor,
        pInicial:req.body.pInicial,

    }
    res.render('cotizacion.html',valores);
})

module.exports=router;